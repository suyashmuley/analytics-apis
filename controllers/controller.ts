import { Request, Response } from "express";
import axios from "axios";
import * as dotenv from 'dotenv';
dotenv.config();

const baseUri: string | undefined = process.env.BASE_URI;
const API_KEY: string | undefined = process.env.API_KEY;

const homePage = async (req:Request, res:Response) => {
    res.send('Hello');
};

// GET API for summary of Traffic of all days
const summaryOfAllTraffic = async (req:Request, res:Response) => {
    const getSummaryOfAllTrafficUri: string = `${baseUri}/totals/summarize/daily?hapikey=${API_KEY}&start=20220201&end=20220302`;
    try {
        const resp = await axios.get(getSummaryOfAllTrafficUri);
        const data: any = resp.data;
        res.json(data);
    } catch (error) {
        console.log(error)
    }
};

// GET API for traffic by source
const trafficBySource = async (req:Request, res:Response) => {
    const trafficBySourceUri = `${baseUri}/sources/total?hapikey=${API_KEY}&start=20220201&end=20220302`;
    try {
        const resp = await axios.get(trafficBySourceUri);
        const data = resp.data;
        res.json(data);
    } catch (error) {
        console.log(error);
    }
};

// GET API for weekly data breakdown
const weeklyBreakDown = async (req: Request, res: Response) => {
    const weeklyBreakDownUri = `${baseUri}/sessions/weekly?hapikey=${API_KEY}&start=20220201&end=20220302`;
    try {
        const resp = await axios.get(weeklyBreakDownUri);
        const data = resp.data;
        res.json(data);
    } catch (error) {
        console.log(error)
    }
};

// GET API for classifying data according to geographic location
const geoLocation = async (req:Request, res:Response) => {
    const geoLocationUri = `${baseUri}/geolocation/total?hapikey=${API_KEY}&start=20220201&end=20220302`;
    try {
        const resp = await axios.get(geoLocationUri);
        const data = resp.data;
        res.json(data);
    } catch (error) {
        console.log(error);
    }
};

// GET API for Daily Breakdown from visits from twitter


// GET API for summary of Data from Tracking Code & similar to top 5 pages with Highest average time
const trackingCodeSummary =async (req:Request, res:Response) => {
    const trackingCodeSummaryUri: string = `${baseUri}/pages/total?hapikey=${API_KEY}&start=20220101&end=20220301`;
    try {
        const resp = await axios.get(trackingCodeSummaryUri);
        const data = resp.data;
        res.json(data);
    } catch (error) {
        console.log(error);
    }
};

export default { homePage, summaryOfAllTraffic, trafficBySource, weeklyBreakDown, geoLocation, trackingCodeSummary };