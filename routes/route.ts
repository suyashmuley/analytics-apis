import { Router } from "express";
import controller from '../controllers/controller';
const router = Router();

router.get('/', controller.homePage);

router.get('/summary', controller.summaryOfAllTraffic);

router.get('/traffic', controller.trafficBySource);

router.get('/weekly-breakdown', controller.weeklyBreakDown);

router.get('/locations', controller.geoLocation);

router.get('/tracking-summary', controller.trackingCodeSummary);

module.exports = router;